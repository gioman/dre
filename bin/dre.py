#!/usr/bin/env python

'''
This scripts reads the current blog list from the database and gets the
stats reading from sitemeter.
'''

# Imports
from django.core.exceptions import ObjectDoesNotExist
import django
import getopt
import sys
import os.path
import pprint

current_dir = os.path.abspath(os.path.dirname(__file__))

sys.path.append(os.path.abspath((os.path.join(current_dir, '../lib/'))))
sys.path.append(os.path.abspath(os.path.join(current_dir, '../dre_django/')))

os.environ['DJANGO_SETTINGS_MODULE'] = 'dre_django.settings'

django.setup()


def usage():
    print('''Usage: %(script_name)s [options]\n
    Commands:
        -r YYYY-MM-DD
        --read_date YYYY-MM-DD
                            Read the DRs from a given date

        --read_range YYYY₁-MM₁-DD₁:YYYY₂-MM₂-DD₂
                            Read the DRs from in a given date range

        --debug YYYY-MM-DD,stage[[,dr],[doc]]
                            Outputs the server responses for a given stage, where stage is:
                                1 - Get the DR list for a given day
                                2 - Get the document list for a given DR and its meta data
                                3 - Get the document body for a given document on a given DR
                            The on stage 1 we get a JSON list of journals, the "dr" option is
                            the index of the journal on that list. Likewise, on stage 2 we get
                            a JSON list of documents, the doc option is the index of the document.

        -d
        --dump              Dump the documents to stdout as a JSON list

        --dump_all          Dump the documents to stdout as a JSON list
                            including the document text where available

        --create_cache YYYY₁-MM₁-DD₁:YYYY₂-MM₂-DD₂
                            Creates (or updates) the cache for the documents
                            in the date range
        --create_cache document_id
                            Updates the cache for the document with id
                            document_id

        -h
        --help              This help screen

    Options:
        Filtering options:

        These options filter the documents to be read. Use these options with
        "--read_date" or "--read_range".

        --no_series_1
        --no_series_2       Ignore documents from series one or two

        --filter_type <type>
                            Process only documents of this type
        --filter_number <number>
                            Process only documents with this number

        Update mode, re-reads the documents:

        The following options will enable the update mode. All these
        options are false by default. You have to pick what you intend
        to update. Use these options with "--read_date" or "--read_range".

        --update_metadata   doc name, number, date, etc
        --update_notes
        --update_digesto
        --update_part
        --update_inforce
        --save_pdf

        NOTE: on every update option if a new document is found it will be
        retrieved completely independent of the value of this update options.

    ''' % {'script_name': sys.argv[0]})

##
# Utils


def create_update_cache(doc):
    logger.debug('Update cache to %d %s %s %s' % (doc.id,
                                                  doc.date, doc.doc_type, doc.number))
    cache = DocumentCache.objects.get_cache_object(doc)
    cache.build_cache()


if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                                   'hr:dv',
                                   ['help',
                                    'read_date=', 'read_range=', 'debug=',
                                    'dump', 'dump_all',
                                    'create_cache=',
                                    'no_series_1', 'no_series_2',
                                    'filter_type=', 'filter_number=',
                                    'update_metadata', 'update_notes',
                                    'update_digesto', 'update_inforce',
                                    'update_part',
                                    'save_pdf',
                                    ])
    except getopt.GetoptError as err:
        print(str(err))
        print()
        usage()
        sys.exit(1)

    # Defaults
    filter_doc = {}  # Document filtering options
    filter_dr = {}  # Journal filtering options
    options = {}    # Document reading options

    # Options
    options['update_notes'] = False
    options['update_metadata'] = False
    options['update_digesto'] = False
    options['update_part'] = False
    options['update_inforce'] = False
    options['save_pdf'] = False

    for o, a in opts:
        # Filter options:
        if o == '--no_series_1':
            filter_dr['no_series_1'] = True
        elif o == '--no_series_2':
            filter_dr['no_series_2'] = True
        elif o == '--filter_type':
            filter_doc['doc_type'] = a.lower()
        elif o == '--filter_number':
            filter_doc['number'] = a.lower()
        # Update options:
        elif o == '--update_notes':
            options['update'] = True
            options['update_notes'] = True
        elif o == '--update_metadata':
            options['update'] = True
            options['update_metadata'] = True
        elif o == '--update_digesto':
            options['update'] = True
            options['update_digesto'] = True
        elif o == '--update_part':
            options['update'] = True
            options['update_part'] = True
        elif o == '--update_inforce':
            options['update'] = True
            options['update_inforce'] = True
        elif o == '--save_pdf':
            options['update'] = True
            options['save_pdf'] = True

    # Commands
    for o, a in opts:
        if o in ('-r', '--read_date'):
            import datetime
            from drescraperv5 import ConnManager
            from drescraperv5 import DREReadDay, DREDocSave
            from drescraperv5 import DREDuplicateError

            try:
                date = datetime.datetime.strptime(a, '%Y-%m-%d')
            except ValueError:
                print('A date in ISO format must be passed to this command')
                sys.exit(1)

            conn = ConnManager()
            day = DREReadDay(date, conn)
            for doc in day.read_index(filter_dr, filter_doc):
                update_obj = DREDocSave(doc, options, conn)
                try:
                    update_obj.save_doc()
                except DREDuplicateError:
                    pass

            sys.exit()

        if o in '--debug':
            import datetime
            from drescraperv5 import ConnManager
            from drescraperv5 import DREReadDay, DREReadJournal, DREReadDoc, DREDocSave

            a = a.split(',')

            try:
                date = datetime.datetime.strptime(a[0], '%Y-%m-%d')
            except ValueError:
                print('A date in ISO format must be passed to this command')
                sys.exit(1)

            try:
                stage = int(a[1])
                if stage not in (1, 2, 3):
                    raise ValueError('Wrong stage')
            except ValueError:
                print('The stage must be 1, 2 or 3')
                sys.exit(1)

            conn = ConnManager()
            day = DREReadDay(date, conn)

            if stage == 1:
                # Get the journal list from a given date
                result = day.get_result(filter_dr, filter_doc)
                for i, dr in enumerate(result):
                    print('### Index', i)
                    pprint.pprint(dr)

            elif stage == 2:
                # Get the document list from a given date and journal
                result = day.get_result(filter_dr, filter_doc)
                try:
                    dr = int(a[2])
                    if dr < 0 and dr > len(result) - 1:
                        raise ValueError('The provided dr does not exist')
                except ValueError:
                    print('The dr must be an integer')
                    sys.exit(1)

                print(80 * '#')
                pprint.pprint(result[dr])

                journal = DREReadJournal(result[dr], date, conn)
                result = journal.get_result()
                for i, doc in enumerate(result):
                    print('### Index', i)
                    pprint.pprint(doc)

            elif stage == 3:
                # Get the document body
                result = day.get_result(filter_dr, filter_doc)
                try:
                    dr = int(a[2])
                    if dr < 0 and dr > len(result) - 1:
                        raise ValueError('The provided dr does not exist')
                except ValueError:
                    print('The dr must be an integer')
                    sys.exit(1)

                print(80 * '#')
                pprint.pprint(result[dr])

                journal = DREReadJournal(result[dr], date, conn)
                result = journal.get_result()
                try:
                    doc = int(a[3])
                    if doc < 0 and doc > len(result) - 1:
                        raise ValueError('The provided dr does not exist')
                except ValueError:
                    print('The doc must be an integer between 0 and %d' % len(result) - 1)
                    sys.exit(1)

                print(80 * '#')
                pprint.pprint(result[doc])

                doc = DREReadDoc(result[doc], journal)
                update_obj = DREDocSave(doc, options, conn)
                result = update_obj.get_result()

                print(80 * '#')
                pprint.pprint(result)

            else:
                print('Could not parse the arguments.')
                sys.exit(1)

            sys.exit()

        elif o == '--read_range':
            import datetime
            from drescraperv5 import ConnManager
            from drescraperv5 import DREReadDay, DREDocSave
            from drescraperv5 import DREDuplicateError

            try:
                date1, date2 = a.split(':')
            except ValueError:
                print('A date range in the format YYYY₁-MM₁-DD₁:YYYY₂-MM₂-DD₂ is needed.')
                sys.exit()

            try:
                date1 = datetime.datetime.strptime(date1, '%Y-%m-%d')
                date2 = datetime.datetime.strptime(date2, '%Y-%m-%d')
            except ValueError:
                print('The dates provided must be in ISO format.')
                sys.exit(1)

            conn = ConnManager()
            date = date1
            while date <= date2:
                day = DREReadDay(date, conn)
                for doc in day.read_index(filter_dr, filter_doc):
                    update_obj = DREDocSave(doc, options, conn)
                    try:
                        update_obj.save_doc()
                    except DREDuplicateError:
                        pass
                date += datetime.timedelta(1)

            sys.exit()

        elif o in ('-d', '--dump'):
            import json
            from dreapp.models import Document

            outfile = sys.stdout
            page_size = 5000

            results = Document.objects.all()

            outfile.write('[\n')
            for i in range(0, results.count(), page_size):
                j = i + page_size
                for doc in results[i:j]:
                    json.dump(doc.dict_repr(), outfile, indent=4)
                    outfile.write(',\n')
            outfile.write(']')

            sys.exit()

        elif o in ('--dump_all',):
            import json
            from dreapp.models import Document

            outfile = sys.stdout
            page_size = 5000

            results = Document.objects.all()

            outfile.write('[\n')
            for i in range(0, results.count(), page_size):
                j = i + page_size
                for doc in results[i:j]:
                    json.dump(doc.dict_repr_all(), outfile, indent=4)
                    outfile.write(',\n')
            outfile.write(']')

            sys.exit()

        elif o == '--create_cache':
            import datetime
            from dreapp.models import Document, DocumentCache
            from drelog import logger
            page_size = 1000
            doc_id = None
            try:
                date1, date2 = a.split(':')
            except ValueError:
                try:
                    doc_id = int(a)
                except ValueError:
                    print('A date range in the format '
                          'YYYY₁-MM₁-DD₁:YYYY₂-MM₂-DD₂ or a document id '
                          'is needed.')
                    sys.exit()
            if doc_id:
                try:
                    doc = Document.objects.get(id=doc_id)
                except ObjectDoesNotExist:
                    print('Doc with id %d does not exist' % doc_id)
                    sys.exit()
                create_update_cache(doc)
            else:
                try:
                    date1 = datetime.datetime.strptime(date1, '%Y-%m-%d')
                    date2 = datetime.datetime.strptime(date2, '%Y-%m-%d')
                except ValueError:
                    print('The dates provided must be in ISO format.')
                    sys.exit(1)
                results = Document.objects.filter(date__gte=date1
                                                  ).filter(date__lte=date2)
                for i in range(0, results.count(), page_size):
                    j = i + page_size
                    print('* Processing documents %d through %d' % (i, j))
                    for doc in results[i:j]:
                        create_update_cache(doc)
            sys.exit()

        elif o in ('-h', '--help'):
            usage()
            sys.exit()

    # Show the help screen if no commands given
    usage()
    sys.exit()
