import os
import datetime
import re
import html
import json
import logging

from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.text import slugify

from bookmarksapp.models import Bookmark
from tagsapp.models import TaggedItem
from notesapp.models import Note

logger = logging.getLogger(__name__)

# Settings
SITE_URL = getattr(settings, 'SITE_URL', 'http://dre.tretas.org')

# Needed for dates < 1899 (read note bellow)
MONTHS = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio',
          'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro',
          'Novembro', 'Dezembro']

##
# Regular expressions - used to detect references to other documents
##

# select distinct doc_type from dreapp_document order by doc_type;
doc_type = (
    'ACÓRDÃO',
    'ACORDO',
    'ACTA',
    'ALVARÁ',
    'ALVARÁ-EXTRACTO',
    'ANÚNCIO',
    'ASSENTO',
    'AUTORIZAÇÃO',
    'AVISO',
    'AVISO-EXTRACTO',
    'CARTA DE CONFIRMAÇÃO E RATIFICAÇÃO DE CONVENÇÃO INTERNACIONAL',
    'CARTA DE LEI',
    'COMUNICAÇÃO DE RENÚNCIA',
    'CONTA',
    'CONTRATO',
    'CONTRATO-EXTRACTO',
    'CONVÉNIO',
    'DECLARAÇÃO',
    'DECLARAÇÃO DE DÍVIDA',
    'DECLARAÇÃO DE RECTIFICAÇÃO',
    'DECRETO',
    'DECRETO LEGISLATIVO REGIONAL',
    'DECRETO LEI',
    'DECRETO REGIONAL',
    'DECRETO REGULAMENTAR',
    'DECRETO REGULAMENTAR REGIONAL',
    'DELIBERAÇÃO',
    'DELIBERAÇÃO-EXTRACTO',
    'DESPACHO',
    'DESPACHO CONJUNTO',
    'DESPACHO-EXTRACTO',
    'DESPACHO MINISTERIAL',
    'DESPACHO NORMATIVO',
    'DESPACHO ORIENTADOR',
    'DESPACHO RECTIFICATIVO',
    'DESPACHO REGULADOR',
    'DIRECTIVA CONTABILÍSTICA',
    'EDITAL',
    'EXTRACTO',
    'INSTRUÇÃO',
    'INSTRUÇÕES',
    'JURISPRUDÊNCIA',
    'LEI',
    'LEI CONSTITUCIONAL',
    'LEI ORGÂNICA',
    'LISTAGEM',
    'LISTA RECTIFICATIVA',
    'LOUVOR',
    'MAPA',
    'MAPA OFICIAL',
    'MOÇÃO',
    'MOÇÃO DE CONFIANÇA',
    'NÃO ESPECIFICADO',
    'NORMA',
    'PARECER',
    'PORTARIA',
    'PORTARIA-EXTRACTO',
    'PROCESSO',
    'PROTOCOLO',
    'RECOMENDAÇÃO',
    'RECTIFICAÇÃO',
    'REGIMENTO',
    'REGULAMENTO',
    'REGULAMENTO INTERNO',
    'RELATÓRIO',
    'RESOLUÇÃO',
    'RESOLUÇÃO DA ASSEMBLEIA NACIONAL',
)

doc_type_str = '|'.join([xi.lower().replace(' ', '(?:\s+|-)')
                         for xi in sorted(doc_type, key=len, reverse=True)])


doc_ref_dtype = r'(?P<doc_type>%s)\s+' % doc_type_str

doc_ref_mid = r'(?P<mid>número\s+|n\.º\s+|n\.\s+|nº\s+|n\s+)'

doc_ref_number = r'(?P<number>(?:\d+\s\d+|(?:[\-A-Z0-9]+)(?:/[\-A-Z0-9]+)*))'

doc_ref_date = r'(?P<date>,\s+de\s+[0-9]+\s+de\s+(?:%(months)s)(?:\s+de\s+\d{2,4})?)?' % {
    'months': '|'.join(MONTHS), }


doc_ref_re_str = doc_ref_dtype + doc_ref_mid + doc_ref_number + doc_ref_date

# Plural

doc_type_plural = (
    'Leis',
    'Decretos-Lei',
    'Decretos-Leis',
    'Portarias',
    'Despachos',
)

doc_type_relation = {
    'leis': 'Lei',
    'decretos-lei': 'Decreto-Lei',
    'decretos-leis': 'Decreto-Lei',
    'portarias': 'Portaria',
    'despachos': 'Despacho',
}

doc_type_str_plural = '|'.join([xi.lower()
                                for xi in sorted(doc_type_plural, key=len, reverse=True)])

doc_ref_dtype_plural = r'(%s)\s+' % doc_type_str_plural

doc_ref_mid_plural = r'(?:números|n\.os|n\.º|n\.|n)\s+'

doc_ref_number_plural = r'(?:\d+\s\d+|[\-A-Z0-9]+(?:/[\-A-Z0-9]+)*)'

doc_ref_date_plural = r'(?:,\s+de\s+[0-9]+\s+de\s+(?:%(months)s)(?:\s+de\s+\d{2,4})?)?' % {
    'months': '|'.join(MONTHS), }

doc_ref_re_str_plural = (doc_ref_dtype_plural + doc_ref_mid_plural +
                         r'((?:' + doc_ref_number_plural + doc_ref_date_plural + r',\s+)+' +
                         r'e\s+' + doc_ref_number_plural + doc_ref_date_plural) + r')'


# Compile the regexes

FLAGS_RE = re.UNICODE | re.IGNORECASE | re.MULTILINE

doc_ref_re = re.compile(doc_ref_re_str, FLAGS_RE)
doc_ref_plural_re = re.compile(doc_ref_re_str_plural, FLAGS_RE)

# regex used to transform the user's search queries
doc_ref_optimize = (
    r'(?ui)(?P<doc_type>%s)(?:\s+número\s+|\s+n\.º\s+|\s+n\.\s+|\s+nº\s+|\s+n\s+|\s+)'
    r'?(?P<number>[/\-a-zA-Z0-9]+)' % doc_type_str)

doc_ref_optimize_re = re.compile(doc_ref_optimize, flags=re.UNICODE)

##
# Utility functions
##


def title(st, exceptions=['a', 'o', 'e', 'de', 'da', 'do']):
    wl = []
    for word in re.sub(r'\s+', ' ', st.lower()).split(' '):
        if word not in exceptions:
            wl.append(word.capitalize())
        else:
            wl.append(word)
    return ' '.join(wl)

##
# Models
##


class Document(models.Model):
    '''
    This table stores the document's metadata.

    We used several sources for the document's text:

    i)   Initially we used the text extracted from the 'texto integral' PDFs;
    ii)  Then we've found out a way of extracting the text from the site itself
         using some special crafted URLs. The text is stored on the
         DocumentText table;

    In any case the html presented is generated by a method of this table.
    '''

    claint = models.IntegerField(unique=True)  # dre.pt site id
    doc_type = models.CharField(max_length=128)

    number = models.CharField(max_length=32)
    emiting_body = models.CharField(max_length=512)
    source = models.CharField(max_length=128)
    part = models.CharField(max_length=2)
    dre_key = models.CharField(max_length=32)
    dr_number = models.CharField(max_length=16, default='')
    series = models.IntegerField(default=1)
    in_force = models.BooleanField(default=True)
    conditional = models.BooleanField(default=False)
    processing = models.BooleanField(default=False)
    date = models.DateField()

    notes = models.CharField(max_length=20480)

    plain_text = models.URLField(default='')
    dre_pdf = models.URLField()
    pdf_error = models.BooleanField(default=False)

    timestamp = models.DateTimeField(default=timezone.now)

    # Reverse generic relations
    bookmarks = GenericRelation(Bookmark)
    tags = GenericRelation(TaggedItem)
    user_notes = GenericRelation(Note)

    # Connection to other documents
    connects_to = models.ManyToManyField('self',  symmetrical=False)

    # Bookmarked?
    def bookmark(self, user):
        '''Return the bookmark associated to this document if it exists'''
        try:
            content_type = ContentType.objects.get_for_model(Document)
            return Bookmark.objects.get(user=user, object_id=self.id,
                                        content_type=content_type)
        except ObjectDoesNotExist:
            return False

    # Connection to other documents
    def out_links(self, all_series=False):
        result = self.connects_to.all()
        if not all_series:
            result = self.connects_to.filter(series__exact=1)
        return result.order_by('date')

    def in_links(self, all_series=False):
        result = self.document_set.all()
        if not all_series:
            result = self.document_set.filter(series__exact=1)
        return result.order_by('date')

    def links(self, all_series=False):
        '''Adjacent vertexes'''
        for vertex in self.out_links(all_series):
            yield vertex
        for vertex in self.in_links(all_series):
            yield vertex

    def out_edges(self):
        for vertex in self.out_links():
            yield (self, vertex)

    def in_edges(self):
        for vertex in self.in_links():
            yield (vertex, self)

    def edges(self):
        '''Incident edges'''
        for vertex in self.out_links():
            yield (self, vertex)
        for vertex in self.in_links():
            yield (vertex, self)

    # Display in lists
    def note_abrv(self):
        if len(self.notes) < 512:
            return self.notes
        else:
            return self.notes[:512] + ' (...)'

    def note_escaped(self):
        return html.escape(self.note_abrv(), quote=True)

    def c_emitting(self):
        '''Comma separated emitting body list'''
        return ', '.join([e.strip().title() for e in self.emiting_body.split(';')])

    # Date methods
    def year(self):
        return self.date.year

    def month(self):
        return self.date.month

    def day(self):
        return self.date.day

    def date_to_index(self):
        return '%d%02d%02d' % (
            self.date.year,
            self.date.month,
            self.date.day, )

    # File methods
    def archive_dir(self):
        return os.path.join(settings.ARCHIVEDIR,
                            '%d' % self.year(),
                            '%02d' % self.month(),
                            '%02d' % self.day())

    def plain_pdf_filename(self):
        return os.path.join(self.archive_dir(), 'plain-%s.pdf' % self.id)

    def dre_pdf_filename(self):
        return os.path.join(self.archive_dir(), 'dre-%s.pdf' % self.id)

    # URLs
    def plain_pdf_url(self):
        return os.path.join(
            '/pdfs',
            '%d' % self.year(),
            '%02d' % self.month(),
            '%02d' % self.day(),
            'plain-%s.pdf' % self.id)

    def dre_pdf_url(self):
        return os.path.join(
            '/pdfs',
            '%d' % self.year(),
            '%02d' % self.month(),
            '%02d' % self.day(),
            'dre-%s.pdf' % self.id)

    def drept_url(self):
        '''
        Returns the dre.pt url pointing to the HTML page.
        '''
        raw_text = self.documenttext_set.all()
        if len(raw_text) != 1:
            return ''
        url = raw_text[0].text_url.split('?')[0]
        if ((url.startswith('https://dre.pt/home/-/dre/') and
            url.endswith('/details/maximized')) or
            url.startswith('https://dre.pt/dre/detalhe/doc/') or
            url.startswith('https://data.dre.pt/eli')):
            return url
        return ''

    # Representation
    def plain_html(self):
        '''Converts the plain_pdf pdf to html'''
        return DocumentCache.objects.get_cache(self)

    def plain_txt(self):
        '''Converts the plain_pdf pdf to txt'''
        html = self.plain_html()
        return re.sub(r'<.*?>', '', html)

    def has_text(self):
        if self.plain_text:
            # PDF with integral text
            return True

        if self.plain_txt().strip():
            # Integral text extracted from html
            return True

        return False

    def title(self):
        # Note: strftime requires years greater than 1899, even though we do not
        # deal with year arithmetic in this method we are forced to find the
        # full month name ourselves.
        return '%s %s, de %d de %s' % (
            title(self.doc_type),
            self.number,
            self.day(),
            MONTHS[self.month()-1])

    def dict_repr(self):
        '''Dictionary representation'''

        return {
            'claint': self.claint,
            'doc_type': self.doc_type,
            'number': self.number,
            'dr_number': self.dr_number,
            'series': self.series,
            'emiting_body': self.emiting_body.split(';'),
            'source': self.source,
            'dre_key': self.dre_key,
            'in_force': self.in_force,
            'conditional': self.conditional,
            'processing': self.processing,
            'date': self.date.isoformat(),
            'notes': self.notes,
            'plain_text': self.plain_text,
            'dre_pdf': self.dre_pdf,
            'pdf_error': self.pdf_error,
            'timestamp': self.timestamp.isoformat(sep=' '),
        }

    def dict_repr_all(self):
        '''Dictionary representation including, if available, the document's
        text'''
        data = self.dict_repr()
        text = self.plain_html()
        data['text'] = text
        return data

    def jsonld_rept_dict(self):
        '''JSON-LD like dict representation'''
        data = {
            "@context": "https://schema.org/",
            "@type": "Legislation",
            "@id": f"{SITE_URL}{self.get_absolute_url()}",
            "name": self.title(),
            "legislationType": self.doc_type,
            "legislationIdentifier": self.number,
            "legislationPassedBy": self.c_emitting(),
            "legislationDate": self.date.isoformat(),
            "legislationLegalForce": ({"@id": "https://schema.org/InForce"}
                                    if self.in_force else
                                    {"@id": "https://schema.org/NotInForce"}),
            "abstract": self.notes,
            "keywords": f"portugal, leis, legislação, diário da república, documento, diploma, { self.doc_type }, { self.date.isoformat() }, { self.date.year }, { self.c_emitting() }",
            "inLanguage": "Portuguese",
            "encoding": [
                {
                        "@type": "LegislationObject",
                        "@id": f"{SITE_URL}{self.get_absolute_url()}",
                        "legislationLegalValue": {"@id": "https://schema.org/UnofficialLegalValue"},
                        "encodingFormat": "HTML"
                },
            ],
        }
        if url := self.drept_url():
            data['encoding'].append({
                "@type": "LegislationObject",
                "@id": url,
                "legislationLegalValue": {"@id": "https://schema.org/OfficialLegalValue"},
                "encodingFormat": "HTML",
                })
        if url := self.dre_pdf:
            data['encoding'].append({
                "@type": "LegislationObject",
                "@id": self.dre_pdf,
                "legislationLegalValue": {"@id": "https://schema.org/DefinitiveLegalValue"},
                "encodingFormat": "PDF",
                })
        return data

    def jsonld_rept(self):
        return json.dumps(self.jsonld_rept_dict())

    def __unicode__(self):
        return self.title()

    # Other
    def slug(self):
        number = self.number.replace(' ', '-')
        title = slugify(self.doc_type)
        date = slugify('de-%d-de-%s' % (self.day(), MONTHS[self.month()-1]))
        title = ('%s-%s-%s' % (title, number, date)
                 if self.number else
                 '%s-%s' % (title, date))
        title = title.replace('/', '-')
        return title

    def get_absolute_url(self):
        return '/dre/%d/%s' % (self.id, self.slug())

    def no_index(self):
        '''
        Returns True if the document is present in the NoIndexDocument table
        '''
        return hasattr(self, 'noindexdocument')


DOCUMENT_VERSION = getattr(settings, 'DOCUMENT_VERSION', 1)


class CacheManager(models.Manager):
    def get_cache_object(self, document):
        try:
            cache = super(CacheManager, self).get_queryset(
            ).get(document=document)
        except ObjectDoesNotExist:
            # Create an empty cache
            cache = DocumentCache()
            cache.document = document
            cache.version = 0
            cache._html = ''
            try:
                # Persist the empty cache
                cache.save()
            except IntegrityError:
                # Another process created the cache for this object. We return the created cache
                # but will not try to save it.
                pass

        return cache

    def get_cache(self, document):
        cache = self.get_cache_object(document)
        return cache.html


class DocumentCache(models.Model):
    '''This table is used to store a cached html representation of the
    Document's text rendered as html. The 'version' field must be equal or
    greater than the settings variable DOCUMENT_VERSION, this way, if it's
    necessary to invalidate the Document cache we only have to increase the
    DOCUMENT_VERSION value.
    '''
    document = models.OneToOneField(Document, on_delete=models.CASCADE)
    version = models.IntegerField()
    _html = models.TextField()
    timestamp = models.DateTimeField(default=timezone.now)

    objects = CacheManager()

    def get_doc_from_title(self, doc_type, number):
        document = Document.objects.filter(
            Q(doc_type__iexact=doc_type.replace(' ', '-')) |
            Q(doc_type__iexact=doc_type.replace('-', ' '))
        ).filter(number__iexact=number.replace(' ', ''))
        if len(document) == 1:
            if self.document != document[0]:
                self.document.connects_to.add(document[0])
            return document[0]
        return None

    def make_links(self,  match):
        doc_type = match.groupdict()['doc_type']
        number = match.groupdict()['number']
        date = match.groupdict()['date']

        # Try to get the referred document from the cache
        doc = self.doc_cache.get(doc_type+number, None)
        # If the doc isn't on the cache, try to get it from the database
        if not doc:
            doc = self.get_doc_from_title(doc_type, number)

        if doc:
            url = doc.get_absolute_url()
            title = html.escape(doc.note_abrv(), quote=True)
            self.doc_cache[doc_type+number] = doc
        else:
            url = '/?q=tipo:%s número:%s' % (doc_type, number.replace(' ', ''))
            title = 'Não temos sumário disponível'
        link_txt = '%s %s%s' % (doc_type, number, date if date else '')

        return '<a href="%s" title="%s">%s</a>' % (url, title, link_txt)

    def make_links_plural(self, match):
        g = match.groups()

        doc_type = doc_type_relation[g[0].lower()]
        n = [xi.strip() for xi in g[1].replace(' e ', ' ').split(',')]
        numbers = list(zip(n[::2], n[1::2]))

        links = []

        for number, date in numbers:
            # Try to get the referred document from the cache
            doc = self.doc_cache.get(doc_type+number, None)
            # If the doc isn't on the cache, try to get it from the database
            if not doc:
                doc = self.get_doc_from_title(doc_type, number)

            if doc:
                url = doc.get_absolute_url()
                title = html.escape(doc.note_abrv(), quote=True)
                self.doc_cache[doc_type+number] = doc
            else:
                url = '/?q=tipo:%s número:%s' % (doc_type, number.replace(' ', ''))
                title = 'Não temos sumário disponível'
            link_txt = '%s, %s' % (number, date if date else '')

            links.append('<a href="%s" title="%s">%s</a>' % (url, title, link_txt))

        links_txt = ', '.join(links[:-1]) + ' e %s' % links[-1]

        return '%s %s' % (g[0], links_txt)

    def build_cache(self):
        logger.info('Creating cache for document id=%s' % self.document.id)
        # Rebuild the cache
        self.version = DOCUMENT_VERSION
        self.timestamp = timezone.now()
        try:
            doc_text = DocumentText.objects.get(document=self.document)
        except ObjectDoesNotExist:
            doc_text = None

        if doc_text:
            # Build the html from the integral text
            html = doc_text.text
            html = re.sub(r'(Artigo \d+\..)\n', r'<strong>\1</strong>\n', html)
            html = re.sub('(CAPÍTULO [IVXLD]+)\n', r'<strong>\1</strong>\n', html)
            html = re.sub('(SECÇÃO [IVXLD]+)\n', r'<strong>\1</strong>\n', html)
            html = re.sub(r'<a .*?>(.*?)</a>', r'\1', html)
            html = html.replace('TEXTO :', '')
            html = html.replace(
                '/application/external/eurolex?',
                'https://dre.pt/application/external/eurolex?')
            if self.document.timestamp > datetime.datetime(2021, 11, 2):
                html = html.replace('\n', '<br><br>\n')
        else:
            # No text to represent
            html = ''

        # Recognize other document names and link to them:
        self.doc_cache = {}
        self.document.connects_to.clear()
        html = doc_ref_re.sub(self.make_links, html)
        html = doc_ref_plural_re.sub(self.make_links_plural, html)

        self._html = html

        try:
            self.save()
        except IntegrityError:
            # Another process created the cache for this object. We return the created cache
            # but will not try to save it.
            pass

    def get_html(self):
        '''This method does the following:
        * Build the html cache for the plain text representation of the document;
        * Creates a list of documents connected to the current document
        '''
        if self.version < DOCUMENT_VERSION or settings.DEBUG:
            self.build_cache()
        return self._html

    html = property(get_html)


class DocumentText(models.Model):
    '''
    This table is used to store raw html retrieved from dre.pt
    '''
    document = models.ForeignKey(Document, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=timezone.now)
    text_url = models.URLField()

    text = models.TextField()


class NoIndexDocument(models.Model):
    '''
    The documents listed on this page will not be indexed by the search engines
    '''
    timestamp = models.DateTimeField(default=timezone.now)
    document = models.OneToOneField(Document, on_delete=models.CASCADE)
