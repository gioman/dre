# Global Imports
from django.contrib import sitemaps

# Local Imports
from dreapp.models import Document


class DocumentSitemap(sitemaps.Sitemap):
    changefreq = "yearly"
    priority = 0.5
    limit = 5000

    def items(self):
        return Document.objects.filter(noindexdocument__isnull=True).order_by('date')

    def lastmod(self, obj):
        return obj.timestamp
