# Global Imports:
import datetime
import re
import urllib.request
import urllib.parse
import urllib.error
import json

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.urls import reverse
from django.db.models import Q, Max, Min
from django.http import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import render_to_string
from django.db import connections

# Local Imports:
from dreapp.forms import BrowseDayFilterForm
from dreapp.forms import QueryForm, BookmarksFilterForm, ChooseDateForm
from dreapp.models import Document
from dreapp.query_utils import parse_query
from settingsapp.models import get_setting
from tagsapp.models import Tag

# Settings
SITE_URL = getattr(settings, 'SITE_URL', 'http://dre.tretas.org')
STATIC_URL = getattr(settings, 'STATIC_URL')


def search(request):
    context = {}
    context['success'] = True
    context['query_modified'] = False
    context['result_count'] = 0

    query = ''
    results = []

    new_data = request.GET.copy()
    form = QueryForm(new_data)

    page = request.GET.get('page', 1)
    try:
        page = int(page)
    except ValueError:
        page = 1

    order = request.GET.get('order', None)
    if order not in ('date', '-date'):
        order = None
    context['order'] = order

    # Was the query modified?
    # T - optimized query (true)
    # F - unmodified query (false)
    mquery = request.GET.get('m', 'T')
    if mquery not in ('T', 'F'):
        mquery = 'T'
    context['mquery'] = mquery

    if form.is_valid():
        query = form.cleaned_data['q']

        results = parse_query(query, mquery == 'T')

        if results.count():  # Did we get results?
            # Yap
            if mquery == 'T':  # Did we optimize the query?
                context['query_modified'] = query
        else:
            # Nope, will try to get results with the user's unmodified query string
            results = parse_query(query, mquery == 'F')

        if order:
            results = results.order_by(order)

        # TODO: Spell correction, for now removed
        # spell_correction = results.spell_correction()
        # spell_correction = spell_correction.get_corrected_query_string()
        # context['spell_correction'] = spell_correction.strip()
        context['spell_correction'] = ''

        context['result_count'] = results.count()
    else:
        form = QueryForm()

    context['form'] = form
    if query:
        context['query'] = '?q=%s' % query
    else:
        context['query'] = ''

    # Setting the pagination
    paginator = Paginator(results, settings.RESULTS_PER_PAGE, orphans=settings.ORPHANS)
    if page < 1:
        page = 1
    if page > paginator.num_pages:
        page = paginator.num_pages

    context['page'] = paginator.page(page)

    object_list = []

    for result in context['page'].object_list:
        if result.object:
            # If we delete records from the database but the Xapian index is
            # not updated, then result.object will be None so we must skip
            # this search result
            no_index = result.object.no_index()
        else:
            context['result_count'] -= 1
            continue
        object_list.append(result.object)
        if no_index:
            context['forgetme'] = True

    if query and not context['result_count']:
        context['success'] = False

    context['page'].object_list = object_list

    return render(request, 'search.html', context)


def browse(request):
    context = {}
    if request.method == 'POST':
        form = ChooseDateForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data['date']
            return redirect(reverse('browse_day',
                                    kwargs={'year': date.year,
                                            'month': date.month,
                                            'day': date.day}))
    else:
        form = ChooseDateForm()
    context['form'] = form
    return render(request, 'browse.html', context)


def today_results(request):
    date = Document.objects.aggregate(Max('date'))['date__max']
    return redirect(reverse('browse_day',
                            kwargs={'year': date.year,
                                    'month': date.month,
                                    'day': date.day}))


def browse_day(request, year, month, day):
    context = {}

    try:
        date = datetime.date(int(year), int(month), int(day))
    except ValueError:
        raise Http404

    ##
    # Filter form
    f = BrowseDayFilterForm(request.GET, date=date)
    context['filter_form'] = f

    query = ''
    doc_type_choices = []
    series = 1
    fdate = date

    if f.is_valid():
        # Filter the results
        query = f.cleaned_data['query']
        doc_type_choices = [int(i) for i in f.cleaned_data['doc_type']]
        series = f.cleaned_data['series']
        try:
            fdate = f.cleaned_data['date'].date()
        except AttributeError:
            pass

        if fdate != date:
            return redirect('%s%s' % (reverse('browse_day',
                                              kwargs={'year': fdate.year,
                                                      'month': fdate.month,
                                                      'day': fdate.day}),
                                      re.sub(r'&page=\d+', '', '?%s' % request.META['QUERY_STRING'])))

    ##
    # Query the document table
    results = Document.objects.filter(date__exact=date)

    if series == 1:
        results = results.filter(series__exact=1)
    elif series == 2:
        results = results.filter(series__exact=2)

    if doc_type_choices:
        t = [f.document_types[i] for i in doc_type_choices]
        results = results.filter(doc_type__in=t)

    if query:
        results = results.filter(
            Q(number__icontains=query) |
            Q(doc_type__icontains=query) |
            Q(emiting_body__icontains=query) |
            Q(source__icontains=query) |
            Q(dre_key__icontains=query) |
            Q(notes__icontains=query)
        )

    results = results.order_by('timestamp')

    ##
    # Dates
    context['prev_date'] = Document.objects.filter(date__lt=date
                                                   ).aggregate(Max('date'))['date__max']
    context['next_date'] = Document.objects.filter(date__gt=date
                                                   ).aggregate(Min('date'))['date__min']
    context['date'] = date

    # Pagination
    page = request.GET.get('page', 1)
    try:
        page = int(page)
    except ValueError:
        page = 1

    paginator = Paginator(results, settings.RESULTS_PER_PAGE, orphans=settings.ORPHANS)
    if page < 1:
        page = 1
    if page > paginator.num_pages:
        page = paginator.num_pages

    context['page'] = paginator.page(page)

    for obj in context['page'].object_list:
        if obj.no_index():
            context['forgetme'] = True
            break

    context['query'] = re.sub(r'&page=\d+', '', '?%s' % request.META['QUERY_STRING'])
    context['query_date'] = re.sub(r'&date=\d+-\d+-\d+', '', '?%s' % request.META['QUERY_STRING'])

    return render(request, 'browse_day.html', context)


def document_redirect(request, docid):
    document = get_object_or_404(Document, pk=docid)
    return redirect(document, permanent=True)


def document_display(request, docid):
    context = {}

    document = get_object_or_404(Document, pk=docid)

    if document.no_index():
        return forgetme(request)

    context['document'] = document
    context['url'] = urllib.parse.quote_plus(SITE_URL + document.get_absolute_url())
    context['text'] = urllib.parse.quote_plus(document.title().encode('utf-8'))

    if request.user.is_authenticated:
        context['show_user_notes'] = get_setting(request.user, 'show_user_notes')

    return render(request, 'document_display.html', context)


def forgetme(request):
    content = render_to_string('forgetme.html', request=request)
    return HttpResponse(content, status=410)


def document_org_pdf(request, docid):
    document = get_object_or_404(Document, pk=docid)

    if not document.dre_pdf:
        raise Http404
#    elif 'getpdf.asp' in document.dre_pdf:
#        url = document.dre_pdf.replace('dig', 'rss')
    elif 'https://dre.pt/application/' in document.dre_pdf or 'https://files.dre.pt' in document.dre_pdf:
        url = document.dre_pdf
    else:
        url = document.dre_pdf_url()

    return redirect(url)


def document_json(request, docid):
    document = get_object_or_404(Document, pk=docid)
    return HttpResponse(json.dumps(document.dict_repr()),
                        content_type='application/json')


def document_jsonld(request, docid):
    '''
    Test the metadata in:
        https://search.google.com/test/rich-results
    '''
    document = get_object_or_404(Document, pk=docid)
    return HttpResponse(json.dumps(document.jsonld_rept_dict()),
                        content_type='application/ld+json')


def bookmark_display(request, userid):
    context = {}
    user = get_object_or_404(User, pk=userid)
    is_owner = user == request.user

    # Bookmark Filter Form
    f = context['filter_form'] = BookmarksFilterForm(request.GET, tags_user=user,
                                                     public_only=user != request.user)

    ##
    # Select the bookmarks
    if is_owner:
        results = Document.objects.filter(bookmarks__user__exact=user)
    else:
        results = Document.objects.filter(Q(bookmarks__user__exact=user) &
                                          Q(bookmarks__public__exact=True))

    if f.is_valid():
        # Filter the results
        order = f.cleaned_data['order']
        invert = f.cleaned_data['invert']
        query = f.cleaned_data['query']
        start_date = f.cleaned_data['start_date']
        end_date = f.cleaned_data['end_date']
        tags = [Tag.objects.get(pk=int(tag_id)) for tag_id in f.cleaned_data['tags']]

        # Date filter
        if start_date:
            results = results.filter(date__gte=start_date)
        if end_date:
            results = results.filter(date__lte=end_date)

        # Query filter
        if query:
            results = results.filter(
                Q(number__icontains=query) |
                Q(doc_type__icontains=query) |
                Q(emiting_body__icontains=query) |
                Q(source__icontains=query) |
                Q(dre_key__icontains=query) |
                Q(notes__icontains=query)
            )

        # Tag filter
        if tags:
            results = results.filter(tags__tag__in=tags)

        # Order:
        sign = '' if invert else '-'

        if order:
            results = results.order_by('%s%s' % (sign,
                                                 'bookmarks__timestamp' if order == 1 else 'date'))
        else:
            results = results.order_by('-bookmarks__timestamp')

    results = results.distinct()

    ##
    # Pagination
    page = request.GET.get('page', 1)
    try:
        page = int(page)
    except:
        page = 1

    paginator = Paginator(results, settings.RESULTS_PER_PAGE, orphans=settings.ORPHANS)
    if page < 1:
        page = 1
    if page > paginator.num_pages:
        page = paginator.num_pages

    # Get the bookmark objects:
    results = list(paginator.page(page).object_list)
    for doc in results:
        doc.bm = doc.bookmark(user)

    # Finish the context:
    context['page'] = paginator.page(page)
    context['results'] = results
    context['query'] = re.sub(r'&page=\d+', '', '?%s' % request.META['QUERY_STRING'])
    context['bookmarks_user'] = user
    context['is_owner'] = is_owner

    return render(request, 'bookmark_display.html', context)


def top(request):
    context = {}

    ##
    # Period to show

    # 0 - statistics since the begining
    # 1 - last month
    # 2 - last week
    # 3 - last year
    period = request.GET.get('period', 2)
    try:
        period = int(period)
    except:
        period = 2
    if period not in (1, 2, 3):
        period = 2
    context['period'] = period

    if period == 1:
        min_date = datetime.datetime.now() - datetime.timedelta(1)
    elif period == 2:
        min_date = datetime.datetime.now() - datetime.timedelta(7)
    else:
        min_date = datetime.datetime.now() - datetime.timedelta(30)

    ##
    # Get the results
    cursor = connections['stats'].cursor()
    query = '''
        select
            count(1) as hits,
            request_path
        from
            statsapp_logline
        where
            response_status = 200 and
            timestamp >= %s and
            request_path ~ '^/dre/[0-9]+/[a-zA-Z\-/0-9]*$'
        group by
            request_path
        order by hits desc
        limit 10
    '''
    cursor.execute(query, [min_date])

    results = []
    for hit in cursor.fetchall():
        doc_id = int(hit[1].split('/')[2])
        try:
            results.append(Document.objects.get(pk=doc_id))
        except ObjectDoesNotExist:
            # This might happen if a document is deleted
            pass

    context['results'] = results

    return render(request, 'top.html', context)


def last(request):
    '''Shows the last 300 documents for the search engine benefict
    '''
    context = {}

    results = Document.objects.all().order_by('-date')[:300]

    context['results'] = results
    return render(request, 'last.html', context)


def view_js(request):
    '''
    This view returns a javascript file with variable initializations
    '''
    js = ['var STATIC_URL="%s";' % STATIC_URL,
          ]
    # Try to get a date:
    date = request.GET.get('date', None)
    if date:
        try:
            date = str(datetime.datetime.strptime(date, '%Y-%m-%d').date())
            js.append('var date="%s";' % date)
        except ValueError:
            pass

    # Return the date
    return HttpResponse('\n'.join(js), content_type='application/javascript')
