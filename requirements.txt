# DO THIS FIRST
#
# Xapian Haystack uses a special installation procedure, see:
# https://github.com/notanumber/xapian-haystack/tree/master 
#
# Activate the venv:
# source <path>/bin/activate
# Install Xapian:
# ./install_xapian.sh <version> (last one 1.4.19 at home)
#
# <version> must be >=1.4.0
#
# Finally, install Xapian-Haystack by running:
#
# pip install xapian-haystack
markdown==3.3.3
Django==3.2.0
django-haystack==3.0
soupsieve==2.0.1
psycopg2==2.8.5
django-simple-captcha==0.5.13
lxml==4.6.1
requests==2.10
gunicorn==20.0.4
gevent==20.9.0
python-dateutil==2.8.1
